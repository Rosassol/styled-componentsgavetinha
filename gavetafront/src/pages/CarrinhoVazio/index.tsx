import React from 'react';
import Header from '../Components/Header/index';

import { View, Text, Image} from 'react-native';
import { Case, ButtonCompre, TextWarning, Texttwo, TextButton, Cart} from './style';

export default function CarrinhoVazio() {
    return (
        <View>
            <Header/>
            <Cart>Meu carrinho</Cart>                 
            <Case> 
                
                <Image source={require('../../../assets/Carrinhoimagem.png')} style={{height: 164, width: 164}}/>
     
                 <TextWarning>Seu carrinho de compras está vazio</TextWarning>
                <Texttwo>Navege pelas nossas ofertas incríveis agora!</Texttwo>

                <ButtonCompre>
                    <TextButton >Compre Agora</TextButton >
                </ButtonCompre> 

            </Case>
        
        </View>
    );

}  