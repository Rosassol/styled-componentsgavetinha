import styled from 'styled-components/native';

export const Case = styled.View`
    flex:1;
    justify-content: column;
    align-items: center;
`
export const Cart = styled.Text`
    display: flex;
    flex-direction:row;
    justify-content:flex-start;
    padding-left: 19px;
    margin-top: 20px;
    margin-bottom:60px;
    color: #313131;
    font-weight: 700;
    font-size: 24px;
    
`
export const TextWarning = styled.Text`
    margin-top: 20px;
    color: black;
    font-weight: 600;
`
export const Texttwo = styled.Text`
    color: black;
    font-weight: 400;
    font-size: 11px;
`
export const ButtonCompre = styled.TouchableOpacity`
    margin-top: 10px;
    height: 45px;
    width: 150px;
    background-color: #690017;
    border-radius: 20px;
`
export const TextButton = styled.Text`
    color: #FFFFFF;
    margin-top: 10px;
    font-size: 15px;
    text-align:center; 
`