import React from "react";
import {Image} from "react-native";
import { Lado } from "./style";

export default function Header(){
    return(
        <Lado>

        <Image source={require('../../../../assets/fotousuario.png')} style={{height: 30, width: 30}} />
        <Image source={require('../../../../assets/logo.svg')} style={{height: 30, width: 30}} />
        <Image source={require('../../../../assets/hamburguer.png')} style={{height: 30, width: 30}} />
        
        </Lado>
    )
}