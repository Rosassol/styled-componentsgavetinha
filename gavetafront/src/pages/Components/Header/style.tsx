import styled from 'styled-components/native';

export const Lado = styled.View`
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
width:100%;
padding: 10px 20px;
` 