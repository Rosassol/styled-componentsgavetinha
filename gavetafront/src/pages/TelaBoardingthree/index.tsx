import React from "react";
import {Image} from "react-native";
import { Cage, Imagegavetinha} from "./style";

export default function boarding(){
    return(
        <Cage>
            <Imagegavetinha>
                <Image source={require('../../../assets/logo.svg')} style={{height: 150, width: 150}} />
                <Image source={require('../../../assets/gavetinha.svg')} style={{height: 100, width: 250}} />
            </Imagegavetinha>
        </Cage>
    )
 }