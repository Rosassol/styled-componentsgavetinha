import React from "react";
import {Image} from "react-native";
import { Cage, Imagegavetinha,Imagelogo} from "./style";

export default function boarding(){
    return(
        <Cage>
            
            <Imagegavetinha>
                <Imagelogo>
                    <Image source={require('../../../assets/logo.svg')} style={{height: 150, width: 150}} />
                </Imagelogo>
                <Image source={require('../../../assets/gavetinhatwo.svg')} style={{height: 60, width: 250}} />
            </Imagegavetinha>
        
        </Cage>
    )
 }