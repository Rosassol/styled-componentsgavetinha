import styled from 'styled-components/native';

export const Cage = styled.View`
    background-color: #6B0C15;
    display: flex;
    flex-direction: column;
    align-items:center;
    justify-content: space-evenly; 
    height:100%;
     
`
export const Imagegavetinha = styled.View`
    display: flex;
    justify-content: center;
    align-items: center;
`

export const Imagelogo = styled.View`
    margin-bottom: 40px;
    background-color: #ffff;
    border-radius: 1000px;
    min-height: 200px;
    min-width: 200px;
    display: flex;
    justify-content: center;
    align-items: center;

`
